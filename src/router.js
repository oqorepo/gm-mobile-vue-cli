import Vue from "vue";
import Router from "vue-router";
import Home from "./views/Home.vue";
import AutosNuevos from "./views/AutosNuevos.vue";

Vue.use(Router);

export default new Router({
  mode: "history",
  base: process.env.BASE_URL,
  routes: [
    {
      path: "/",
      name: "home",
      component: Home
    },
    {
      path: "/autos-nuevos",
      name: "autos-nuevos",
      component: AutosNuevos
    },
    {
      path: "/about",
      name: "about",
      component: () =>
        import(/* webpackChunkName: "about" */ "./views/About.vue")
    }
  ]
});
