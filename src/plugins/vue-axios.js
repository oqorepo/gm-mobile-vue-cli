import Vue from "vue";
import axios from "axios";
import VueAxios from "vue-axios";
const baseURL = "https://www.guillermomorales.cl/wp-json/wp/v2";
axios.defaults.baseURL = baseURL;
Vue.use(VueAxios, axios);
