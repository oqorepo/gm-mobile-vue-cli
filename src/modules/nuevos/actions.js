export async function fetchBrands({ commit }) {
  try {
    const { data } = await Vue.axios({ url: "/marcas" });
    commit("nuevos/setBrands", data, {
      root: true
    });
  } catch (error) {
    console.log(error.message);
  }
}
